export class Player {
  name: string = '';
  character: string = '';
  weapon: string = 'Épée';
  attack: string = '5';
  defense: string = '5';
  action: string = '';
}
