import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Player } from './player.model';
import { HttpClient } from '@angular/common/http';
import { Classe } from './classe.model';
import { IconNamesEnum } from 'ngx-bootstrap-icons';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.css',
})
export class AppComponent implements OnInit {
  constructor(private httpClient: HttpClient) { }

  messages: string[] = [];
  players: Player[] = [];
  classes: Classe[] = [];
  randomClasses: string[] = [];
  playersShaking: boolean[] = [false, false, false];

  gameHasStarted: boolean = false;
  waitingForApi: boolean = false;
  musicStarted: boolean = false;
  music: HTMLAudioElement = new Audio();

  ngOnInit(): void {
    this.players.push(new Player());
    this.players.push(new Player());
    this.players.push(new Player());

    this.classes.push({ name: 'Guerrier', image: '/assets/gifs/warrior.gif' });
    this.classes.push({ name: 'Mage', image: '/assets/gifs/mage.gif' });
    this.classes.push({ name: 'Garde', image: '/assets/gifs/spear.gif' });
    this.classes.push({ name: 'Élu', image: '/assets/gifs/jesus.gif' });
    this.classes.push({ name: 'Assassin', image: '/assets/gifs/assassin.gif' });

    this.randomClasses = this.classes
      .map((c) => c.image)
      .sort(() => Math.random() - 0.5)
      .slice(0, 3);

    this.messages.push('Bienvenue dans le monde de Donjon et Dragon ! Vous allez devoir choisir un personnage chacun pour vous lancer dans une aventure épique. Bonne chance à vous !');

    this.music.volume = 0;
  }

  canPlay(): boolean {
    return this.players.every((p) => p.name && p.character) && !this.waitingForApi && ((this.gameHasStarted && this.players.every((p) => p.action)) || !this.gameHasStarted);
  }

  play() {
    new Audio('/assets/sounds/button-sound-effect.mp3').play();

    const firstMove = !this.gameHasStarted;

    this.gameHasStarted = true;
    this.waitingForApi = true;

    this.httpClient.post('http://localhost:4000/game', { players: this.players, first: firstMove }).subscribe((res: any) => {
      this.handlePlayerChange(res.prompt.scenario ?? 'Une erreur est survenue');
    });
  }

  handlePlayerChange(scenario: string) {
    this.waitingForApi = false;
    this.messages.push(scenario);
    this.players.forEach((p) => (p.action = ''));

    this.playersShaking.forEach((_, i) => {
      setTimeout(() => (this.playersShaking[i] = true), Math.random() * 2000);
    });

    setTimeout(() => {
      this.playersShaking = [false, false, false];
    }, 3000);
  }

  getCharacter(player: Player) {
    return this.classes.find((c) => c.name === player.character)?.image ?? '/assets/gifs/questionmark.gif';
  }

  getCharacters() {
    let characters = this.players.map((p) => this.getCharacter(p));

    if (characters.includes('/assets/gifs/questionmark.gif')) characters = this.randomClasses;

    return characters;
  }

  getMusicIcon(): IconNamesEnum {
    return this.music.volume === 0 ? IconNamesEnum.VolumeMute : IconNamesEnum.VolumeUp;
  }

  changeMusicState() {
    if (!this.musicStarted) {
      this.musicStarted = true;
      this.music = new Audio(`/assets/musics/${Math.floor(Math.random() * 24 + 1)}.opus`);
      this.music.loop = true;
      this.music.play();
    }
    else {
      this.music.volume = this.music.volume === 0 ? 1 : 0;
    }
  }
}
