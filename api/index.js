const app = require("express")();
const fs = require("fs");
const bodyParser = require("body-parser");
const axios = require("axios");
const extract = require("extract-json-from-string");
const cors = require("cors");

const basePrompt = fs.readFileSync("./prompt.txt", "utf8").toString();
const actionPrompt = fs.readFileSync("./prompt_action.txt", "utf8").toString();

const gptApi = axios.create({
  baseURL: "http://localhost:3000",
});

app.use(bodyParser.json());

app.use(cors({ origin: "*" }));

app.post("/game", (req, res) => {
  const players = req.body.players;
  const first = req.body.first;
  console.log(players);

  // we call the api on localhost:3000/message to get the first prompt

  const prompt = getPrompt(players, first);

  gptApi.post("/message", { message: prompt }).then((response) => {
    console.log("Response :",  response.data);
    console.log("Response Data cleaned:",  response.data.replace(/\n/g, ""));

    const parsed = extract(response.data.replace(/\n/g, ""))[0];
    console.log("Parsed : ", parsed);

    res.json({ prompt: parsed });
  }, (error) => {
    console.error("Error : ", error);
    res.status(500).send("Error");
  });
});

app.listen(4000, () => {
  console.log("Server is running on port 4000");
});

const getPrompt = (players, first) => {
  const prompt = first ? basePrompt : actionPrompt;
  return prompt.replace("<<<PLAYERS>>>", JSON.stringify(players));
};
