# Volcamp 2024

Ce projet est un POC pour l'animation de Volcamp 2024.

Ceci est un test pour voir si un LLM comme GPT 3.5 peut être utilisé pour animer un événement sur le thème du jeu de rôle. Il n'est pas utilisable en l'état pour une production ou pour une utilisation en dehors des tests.

## Bien commencer

Pour commencer, il faut que vous ayez l'API de ChatGPT qui tourne sur votre machine. Pour cela, il faut suivre les instructions du repo suivant : [maxd6363/chatgpt-api](https://gitlab.com/maxd6363/chatgpt-api).


Pour lancer le backend sur le port 4000, il suffit de lancer les commandes suivantes :

```bash
cd api
npm install
npm run start
```

Pour lancer le frontend, il suffit de lancer les commandes suivantes :

```bash
cd front/volcamp-2024
npm install
npm run start
```


## Architecture

La totalité du projet ressemble à ceci :

<img src="https://i.ibb.co/xgYfXyS/image-2024-04-07-200702981.png">